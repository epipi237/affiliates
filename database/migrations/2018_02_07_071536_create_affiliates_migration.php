<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('affiliates', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id',20)->unsigned();
            $table->text('rate');
            $table->text('rate_type');
            $table->mediumText('payment_email');
            $table->text('status');
            $table->string('logo',255);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('affiliates');
    }
}
