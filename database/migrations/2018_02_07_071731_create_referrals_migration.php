<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('referrals', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('visit_id',20)->unsigned();
            $table->bigInteger('affiliate_id',20)->unsigned();
            $table->longText('description');
            $table->text('status');
            $table->mediumText('amount');
            $table->char('currency',3);
            $table->longText('custom');
            $table->text('context');
            $table->mediumText('reference');
            $table->date('date');
            $table->string('campaign',50);
            $table->mediumText('products');
            $table->timestamps();
            $table->foreign('affiliate_id')->references('id')->on('affiliates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('referrals');
    }
}
