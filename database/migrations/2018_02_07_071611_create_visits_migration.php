<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('visits', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('affiliate_id',20)->unsigned();
            $table->bigInteger('referral_id',20)->unsigned();
            $table->mediumText('url');
            $table->mediumText('referral');
            $table->text('ip');
            $table->date('date');
            $table->string('campaign',50);
            $table->string('context',50);
            $table->timestamps();
            $table->foreign('affiliate_id')->references('id')->on('affiliates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visits');
    }
}
