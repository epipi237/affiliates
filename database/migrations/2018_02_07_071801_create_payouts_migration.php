<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayoutsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('payouts', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('affiliate_id',20)->unsigned();
            $table->bigInteger('visit_id',20)->unsigned();
            $table->mediumText('referrals');
            $table->mediumText('amount');
            $table->text('payment_method');
            $table->text('status');
            $table->date('date');
            $table->bigInteger('owner',20);
            $table->timestamps();
            $table->foreign('affiliate_id')->references('id')->on('affiliates')->onDelete('cascade');
        });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payouts');
    }
}
