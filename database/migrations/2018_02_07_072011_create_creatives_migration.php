<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreativesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('creatives', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('description');
            $table->text('name');
            $table->string('url',255);
            $table->text('text');
            $table->string('image',255);
            $table->text('status');
            $table->date('date');
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('creatives');
    }
}
