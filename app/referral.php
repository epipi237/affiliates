<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class referral extends Model
{
    public function affiliate(){
    	return $this->belongsTo('App\affiliate');
    }
    public function visits(){
    	return $this->hasMany('App\visit');
    }
}
