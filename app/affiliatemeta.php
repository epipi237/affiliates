<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class affiliatemeta extends Model
{
    protected $table="affiliatemetas";
    
    public function affiliate(){
    	return $this->belongsTo('App\affiliate');
    }
}
